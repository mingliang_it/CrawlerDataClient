﻿using CrawlerComm.Handler;
using CrawlerModel.Meitu;
using CrawlerService.Meitu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrawlerData
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            //岗前检测异常发送群聊消息
            UpdateFrmHandler.UpdateUI += UpdateUIFuc;
        }

        /// <summary>
        /// 更新界面委托方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="updateUIModel"></param>
        private void UpdateUIFuc(object sender, UpdateUIModel updateUIModel )
        {
            try
            {

                //改变界面UI中的值
                this.Invoke((Action)delegate
                {
                    UpdateUIBiz(updateUIModel);
                });

            }
            catch (Exception ex)
            {
              
            }
        }
        /// <summary>
        /// 更新UI界面业务
        /// </summary>
        /// <param name="updateUIModel"></param>
        private void UpdateUIBiz(UpdateUIModel updateUIModel)
        {
            try
            {
                //分类
                tbCategory.Text = updateUIModel.Category;
                //排名
                tbNo.Text = updateUIModel.beauty.No;
                //姓名
                tbName.Text = updateUIModel.beauty.Name;
                //人气值
                tbPopularity.Text = updateUIModel.beauty.Popularity;
                //图片地址
                tbUrl.Text = updateUIModel.beauty.ImageUrl;
                //图片
                WebClient client = new WebClient();
                string imageUrl = updateUIModel.beauty.ImageUrl;// 要显示的网络图片的URL
                byte[] imageBytes = client.DownloadData(imageUrl);
                Image img = Image.FromStream(new MemoryStream(imageBytes));
                picBox.Image = img;
                //爬取数量
                lbNumber.Text = updateUIModel.DownloadNumber.ToString();
                
            }
            catch (Exception ex)
            {

            }
        }
        /// <summary>
        /// 开始按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private  void btStart_Click(object sender, EventArgs e)
        {
            StartCrawler();
        }

        /// <summary>
        /// 开始爬取
        /// </summary>
        public async void StartCrawler()
        {
            try
            {
                MeituParseHtmService meituParseHtml = new MeituParseHtmService();
                await meituParseHtml.StartAsync();
            }
            catch (Exception ex )
            {

            }
        
        }
    }
}
