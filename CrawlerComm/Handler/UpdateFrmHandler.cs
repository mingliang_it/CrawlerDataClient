﻿#region << 版 本 注 释 >>
/*----------------------------------------------------------------
 * 创建者：王明亮
 * 创建时间：2023/8/11 11:28:24
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 版 本 注 释 >>

using CrawlerModel.Meitu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrawlerComm.Handler
{
    /// <summary>
    /// UpdateFrmHandler 更新UI事件委托
    /// </summary>
    public class UpdateFrmHandler
    {
        /// <summary>
        /// 更新下载界面事件
        /// </summary>
        public static event EventHandler<UpdateUIModel> UpdateUI;
        public static void OnUpdateUI(UpdateUIModel updateUIModel)
        {
            UpdateUI?.Invoke(null, updateUIModel);
        }
    }
}
