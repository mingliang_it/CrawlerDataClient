﻿#region << 版 本 注 释 >>
/*----------------------------------------------------------------
 * 创建者：王明亮
 * 创建时间：2023/8/4 9:58:03
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 版 本 注 释 >>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrawlerModel.Meitu
{

    /// <summary>
    ///  爬取的专区字段实体
    /// </summary>
    public class BeautyZone
    {

        /// <summary>
        /// 专区标题
        /// </summary>
        public string Tittle { get; set; }
        /// <summary>
        /// 专区每种类型美女排行榜
        /// </summary>
        public List<EveryCategoryBeautyTop> categoryBeauties { get; set; }

    }

    /// <summary>
    ///  每分类美女排行榜
    /// </summary>
    public class EveryCategoryBeautyTop
    {
        /// <summary>
        /// 类别
        /// </summary>
         public string Category { get; set; }
        /// <summary>
        /// 每种类型排行榜
        /// </summary>
        public List<Beauty> beauties { get; set; }
        
    }

    /// <summary>
    /// 美女排行信息
    /// </summary>
    public  class Beauty
    {
        /// <summary>
        /// 排行
        /// </summary>
        public string  No { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public  string Name { get; set; }
        /// <summary>
        /// 热度
        /// </summary>
        public string  Popularity { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageUrl { get; set; }
    }
}
