﻿#region << 版 本 注 释 >>
/*----------------------------------------------------------------
 * 创建者：王明亮
 * 创建时间：2023/8/11 8:35:43
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 版 本 注 释 >>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrawlerModel.Meitu
{
    /// <summary>
    /// OtherConfig 的摘要说明
    /// </summary>
    public class OtherConfig
    {
        /// <summary>
        /// 子类类别XPath
        /// </summary>
        public const string ChildCategoriesXPath = @"//*[@id=""tab1_div_{0}""]/ul/li";

        /// <summary>
        /// 机构类别
        /// </summary>
        public const string OrganizationDivXPath = @"//*[@id=""tab1_div_5""]/div/a";

        /// <summary>
        /// 分页盒子所在的XPath
        /// </summary>
        public const string PageDivXPath = @"//*[@id=""pages""]/a";

        /// <summary>
        /// 写真列表Li标签集合
        /// </summary>
        public const string PhotoAlbumsLisXPath = @"/html/body/div[1]/div[2]/ul/li";
    }
}
