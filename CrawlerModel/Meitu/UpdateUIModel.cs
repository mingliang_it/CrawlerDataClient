﻿#region << 版 本 注 释 >>
/*----------------------------------------------------------------
 * 创建者：王明亮
 * 创建时间：2023/8/13 16:42:12
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 版 本 注 释 >>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrawlerModel.Meitu
{
    /// <summary>
    /// UpdateUIModel 更新UI界面需要的字段
    /// </summary>
    public class UpdateUIModel
    {
        /// <summary>
        /// 下载的数量
        /// </summary>
        public int DownloadNumber { get; set; }
        /// <summary>
        /// 分类
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 美女写真实体
        /// </summary>
        public Beauty beauty =new Beauty();
        

    }
}
