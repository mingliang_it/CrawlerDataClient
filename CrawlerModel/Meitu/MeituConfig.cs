﻿#region << 版 本 注 释 >>
/*----------------------------------------------------------------
 * 创建者：王明亮
 * 创建时间：2023/8/4 10:08:06
 * 版本：V1.0.0
 * 描述：
 *
 * ----------------------------------------------------------------
 * 修改人：
 * 时间：
 * 修改说明：
 *
 * 版本：V1.0.1
 *----------------------------------------------------------------*/
#endregion << 版 本 注 释 >>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CrawlerModel.Meitu
{
    /// <summary>
    /// MeituConfig 的摘要说明
    /// </summary>
    public class MeituConfig
    {
        /// <summary>
        /// 存放数据地址
        /// </summary>
        public const string JsonDataPath = "meitu/data";
        /// <summary>
        /// 爬取美图首页地址
        /// </summary>
        public const string Url = "https://www.meitu131.com";
        /// <summary>
        /// 专区XPath
        /// </summary>
        public const string ZoneXPath = @"/html/body/div[10]/div[2]/div";
        /// <summary>
        /// 专区名称XPath
        /// </summary>
        public const string ZoneNameXPath = @"/html/body/div[10]/div[1]/ul/li";
        /// <summary>
        /// 排行榜
        /// </summary>
        public const string TopXPath = @"/html/body/div[10]/div[2]/div[{0}]/dl";
        /// <summary>
        /// 人员隶属种类
        /// </summary>
        public const string PersonCategoryXPath = @"/html/body/div[10]/div[2]/div[{0}]/dl/dt";
        /// <summary>
        /// 人员
        /// </summary>
        public const string PersonXPath = @"/html/body/div[10]/div[2]/div[{0}]/dl/dd";
        /// <summary>
        /// 排行
        /// </summary>
        public const string NoXPath = @"/html/body/div[3]/div[1]/ul/li";
        /// <summary>
        /// 姓名
        /// </summary>
        public const string NameXPath = @"/html/body/div[3]/div[1]/ul/li";
        /// <summary>
        /// 热度
        /// </summary>
        public const string PopularityXPath = @"/html/body/div[3]/div[1]/ul/li";
        /// <summary>
        /// 图片地址
        /// </summary>
        public const string ImageUrlXPath = @"/html/body/div[3]/div[1]/ul/li";

    }
}
